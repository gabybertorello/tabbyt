﻿using Antlr4.Runtime;
using System;
using System.IO;
using System.Runtime.Serialization;

namespace TabbyT.src.compiler.Error
{
  public class ThrowingErrorListener : BaseErrorListener {
    public static ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

    public override void SyntaxError(TextWriter output, IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e) {
      throw new ParsingException($"line {line}:{charPositionInLine} {msg}");
    }
  }

  [Serializable]
  public class ParsingException : Exception {
    public string msg { get; }

    public ParsingException(string message) {
      msg = message;
    }
  }
}
