﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TabbyT.src.compiler.Error {
  class Error {
    public String message { get; }

    public Error(String m) {
      message = m;
    }
  }

  class ErrorList {

    private static ErrorList errorList;
    public List<Error> errors { get; }

    public static ErrorList getErrorList() {
      if (errorList == null) {
        errorList = new ErrorList();
      }
      return errorList;
    }


    private ErrorList() {
      errors = new List<Error>();
    }



    public void Add(String message) {
      errors.Add(new Error(message));
      Console.WriteLine(message);
      Environment.Exit(-1);
    }
    
    public void Clear() {
      errors.Clear();
    }

    public Boolean isEmpty() {
      return ( !errors.Any() );
    }

  }
}
