﻿using System;

namespace TabbyT.src.compiler {

  class Instruction {
    public Node node { get; }
    public InstructionSet instruction { get; }

    public Instruction(Node n, InstructionSet i) {
      node = n;
      instruction = i;
    }

    public enum InstructionSet {
      //Initialization
      PROGRAMINIT,

      //Create Map Entry
      INITMAPDATA,

      //
      OBJECTINDEXCONSTANTS,

      //Create Object Bodys
      INITOBJECTBODY,
      CREATEOBJECTBODY,
      ENDOBJECTBODYORENTRY,

      //Create Object Entry
      INITOBJECTENTRY,
      CREATEOBJECTENTRY,

      //GenerateNoiseMap
      GENERATENOISEMAP,

      //
      GENERATEMAPVALUES,
      //
      GENERATEOBJECTSINIT,
      GENERATEOBJECTSCASE,
      GENERATEOBJECTSEND,
      GENERATEVALUES,
      FILTEROBJECTS,
      // falta FilterObjects
      GENERATEPOINTS,
      GENERATESETSGETSANDSTRUCTS,
      CREATECLASSMAPOBJECT,
      CREATECLASSOBEJCTS,
      CREATERANDOMNUMBERGENERATOR,

      //
      COMMA,

      //Default
      NULL
    }

    public static InstructionSet GetType(String word) {
      switch (word) {
        case "init":
          return InstructionSet.PROGRAMINIT;
        case "constructor":
          return InstructionSet.INITMAPDATA;
        case "varrandom":
          return InstructionSet.INITOBJECTBODY;
        case "mapgen":
          return InstructionSet.ENDOBJECTBODYORENTRY;
        default:
          return InstructionSet.NULL;
      }
    }
  }

}

