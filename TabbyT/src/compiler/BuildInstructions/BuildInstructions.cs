﻿using System;
using System.Collections.Generic;
using TabbyT.src.compiler.Error;
using static TabbyT.src.compiler.Instruction;

/// <summary>
/// Given an AST, intermediate instructions are created to
/// simplify the creation of the corresponding source code. 
/// Instruction: (node n, InstructionSet i)
/// </summary> 
namespace TabbyT.src.compiler {
  internal class BuildInstructions : AstVisitor<List<Instruction>> {

    public BuildInstructions() {
    }

    /// <summary>
    /// Given a Program node (AST), return the corresponding
    /// list of intermediate instructions.
    /// </summary> 
    public override List<Instruction> Visit(Program p) {
      List<Instruction> instructions = new List<Instruction>();

      instructions.Add(new Instruction(p, InstructionSet.PROGRAMINIT));

      List<ObjEntry> ObjectEntries = p.ObjectEntries;
      // Crea las constantes de los indices de los objEntries
      foreach (ObjEntry objEntry in ObjectEntries) {
        instructions.Add(new Instruction(objEntry, InstructionSet.OBJECTINDEXCONSTANTS));
      }

      // Create mapEntry
      MapEntry map = p.mapEntry;
      instructions.AddRange((List<Instruction>)Visit(map));

      // Crea variables de cada objentry
      foreach (ObjEntry objEntry in ObjectEntries) {
        instructions.Add(new Instruction(objEntry, InstructionSet.INITOBJECTBODY));
        instructions.AddRange((List<Instruction>)Visit(objEntry, "CreateVariables"));
        instructions.Add(new Instruction(p, InstructionSet.ENDOBJECTBODYORENTRY));
      }

      // Crea cada objData
      instructions.Add(new Instruction(p, InstructionSet.INITOBJECTENTRY));
      foreach (ObjEntry objEntry in ObjectEntries) {
        instructions.AddRange((List<Instruction>)Visit(objEntry, "CreateObjectData"));
        instructions.Add(new Instruction(p, InstructionSet.COMMA));
      }
      // Remueve la coma en el caso de que sea el ultimo objBody
      removeLastComma(ObjectEntries.Count, instructions);  

      instructions.Add(new Instruction(p, InstructionSet.ENDOBJECTBODYORENTRY));

      instructions.Add(new Instruction(p, InstructionSet.GENERATENOISEMAP));

      instructions.Add(new Instruction(p.mapEntry, InstructionSet.GENERATEMAPVALUES));

      instructions.Add(new Instruction(p, InstructionSet.GENERATEOBJECTSINIT));

      // Create case dentro de GenerateObjects      
      foreach (ObjEntry objEntry in ObjectEntries){
        instructions.Add(new Instruction(objEntry, InstructionSet.GENERATEOBJECTSCASE));
      }
      instructions.Add(new Instruction(p, InstructionSet.GENERATEOBJECTSEND));

      // Create GenerateValues and filterObjets           
      foreach (ObjEntry objEntry in ObjectEntries) {
        instructions.AddRange((List<Instruction>)Visit(objEntry, "CreateGenerateValuesForEachObject"));
      }
      instructions.Add(new Instruction(p, InstructionSet.FILTEROBJECTS));
      instructions.Add(new Instruction(p, InstructionSet.GENERATEPOINTS));
      instructions.Add(new Instruction(p, InstructionSet.GENERATESETSGETSANDSTRUCTS));

      instructions.Add(new Instruction(p, InstructionSet.CREATECLASSMAPOBJECT));

      foreach (ObjEntry objEntry in ObjectEntries) {
        instructions.Add(new Instruction(objEntry, InstructionSet.CREATECLASSOBEJCTS));
      }

      instructions.Add(new Instruction(p, InstructionSet.CREATERANDOMNUMBERGENERATOR));

      return instructions;
    }

    /// <summary>
    /// Given a MapEntry node (AST), return the corresponding
    /// list of intermediate instructions.
    /// </summary> 
    public override List<Instruction> Visit(MapEntry m) {
      List<Instruction> instructions = new List<Instruction>();
      instructions.Add(new Instruction(m, InstructionSet.INITMAPDATA));

      instructions.Add(new Instruction(m, InstructionSet.INITOBJECTBODY));
      foreach (ObjBody objBody in m.ObjectBodies) {
        instructions.AddRange((List<Instruction>)Visit(objBody));
        instructions.Add(new Instruction(m, InstructionSet.COMMA));
      }
      // Remueve la coma en el caso de que sea el ultimo objBody
      removeLastComma(m.ObjectBodies.Count, instructions);
      instructions.Add(new Instruction(m, InstructionSet.ENDOBJECTBODYORENTRY));

      return instructions;
    }

    /// <summary>
    /// Given an ObjEntry node (AST) and a typeOfEntry (CreateVariables, CreateObjectData
    /// o CreateGenerateValuesForEachObject), return the corresponding list of intermediate instructions.
    /// </summary> 
    public override List<Instruction> Visit(ObjEntry oe, string typeOfEntry) {
      List<Instruction> instructions = new List<Instruction>();

      if (typeOfEntry.Equals("CreateVariables"))
      {
        foreach (ObjBody objBody in oe.objectBodies)
        {
          instructions.AddRange((List<Instruction>)Visit(objBody));
          instructions.Add(new Instruction(oe, InstructionSet.COMMA));
        }
        removeLastComma(oe.objectBodies.Count, instructions);
      }
      
      else if (typeOfEntry.Equals("CreateObjectData")) {
        instructions.Add(new Instruction(oe, InstructionSet.CREATEOBJECTENTRY));
      } else if (typeOfEntry.Equals("CreateGenerateValuesForEachObject")) {
        instructions.Add(new Instruction(oe, InstructionSet.GENERATEVALUES));
      }

      return instructions;
    }

    /// <summary>
    /// Removes the last comma of the specified list of intermediate instructions (comma is an instruction)
    /// </summary> 
    private static void removeLastComma(int count, List<Instruction> instructions){
      if (count != 0){
        instructions.RemoveAt(instructions.Count - 1);
      }
    }

    /// <summary>
    /// Given an ObjBody node (AST) return the corresponding 
    /// list of intermediate instructions.
    /// </summary> 
    public override List<Instruction> Visit(ObjBody ob) {
      List<Instruction> instructions = new List<Instruction>();
      instructions.Add(new Instruction(ob, InstructionSet.CREATEOBJECTBODY));

      return instructions;
    }

    public override List<Instruction> Visit(Range r) {
      return null;
    }
  }
}