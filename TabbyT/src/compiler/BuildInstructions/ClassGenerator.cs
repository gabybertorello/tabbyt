﻿using System;
using System.Collections.Generic;
using System.IO;
using static TabbyT.src.compiler.Instruction;
using TabbyT.src.compiler.Error;
using static TabbyT.src.compiler.Type;
using System.Text;

namespace TabbyT.src.compiler {
    class ClassGenerator {
        const String tab = "  ";
        int objectIndex = 0;
        IDictionary<InstructionSet, Action<Node>> dict;
        List<String> codeLinesForNoise;
        List<String> codeLinesForMapObject;
        List<String> codeLinesForRandomNumberGenerator;
        ErrorList errorList;

        public ClassGenerator(List<Instruction> inst, string output) {
            dict = new Dictionary<InstructionSet, Action<Node>>() {
                { InstructionSet.PROGRAMINIT, ProgramInit },
                { InstructionSet.INITMAPDATA, InitMapData },
                { InstructionSet.OBJECTINDEXCONSTANTS, ObjectIndexConstants },
                { InstructionSet.INITOBJECTBODY, InitObjectBody },
                { InstructionSet.CREATEOBJECTBODY, CreateObjectBody },
                { InstructionSet.ENDOBJECTBODYORENTRY, EndObjectBodyOrEntry },
                { InstructionSet.INITOBJECTENTRY, InitObjectEntry },
                { InstructionSet.CREATEOBJECTENTRY, CreateObjectEntry },
                { InstructionSet.GENERATENOISEMAP, GenerateNoiseMap },
                { InstructionSet.GENERATEOBJECTSINIT , GenerateObjectsInit },
                { InstructionSet.GENERATEOBJECTSCASE, GenerateObjectsCase },
                { InstructionSet.GENERATEOBJECTSEND, GenerateObjectsEnd },
                { InstructionSet.GENERATEMAPVALUES, GenerateMapValues },
                { InstructionSet.GENERATEVALUES, GenerateValues },
                { InstructionSet.FILTEROBJECTS, FilterObjects },
                { InstructionSet.GENERATEPOINTS, GeneratePoints },
                { InstructionSet.GENERATESETSGETSANDSTRUCTS, GenerateSetsGetsAndStructs },
                { InstructionSet.CREATECLASSMAPOBJECT, CreateClassMapObject },
                { InstructionSet.CREATECLASSOBEJCTS, CreateClassObjects },
                { InstructionSet.CREATERANDOMNUMBERGENERATOR, CreateRandomNumberGenerator },
                { InstructionSet.COMMA, Comma }
            };
            errorList = ErrorList.getErrorList();
            codeLinesForNoise = new List<string>();
            codeLinesForMapObject = new List<string>();
            codeLinesForRandomNumberGenerator = new List<string>();
            GenerateCSharpClass(inst);
            WriteFile(output+"/Noise.cs", codeLinesForNoise.ToArray());
            WriteFile(output+"/MapObject.cs",codeLinesForMapObject.ToArray());
            WriteFile(output+"/RandomNumberGenerator.cs", codeLinesForRandomNumberGenerator.ToArray());
        }

        private void WriteFile(string output,string[] lines){
          try {
             System.IO.File.WriteAllLines(output, lines);
          } catch (IOException ) {
            errorList.Add("Dirección incorrecta: " + output);
          }
        }

        private void GenerateCSharpClass(List<Instruction> inst) {
            foreach (var i in inst) {
                ExecuteCode(i);
            }
        }

        private void ExecuteCode(Instruction code)
        {
            Action<Node> action;

            if (dict.TryGetValue(code.instruction, out action)) {
                action(code.node);
            } else {
                errorList.Add("Could not find the specified key.");
            }
        }

        private void ProgramInit(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                    .AppendLine("using UnityEngine; ")
                    .AppendLine("using System.Collections;")
                    .AppendLine("using System.Collections.Generic;")
                    .AppendLine()
                    .AppendLine("public static class Noise {")
                    .AppendLine("  private static RandomNumberGenerator rngNoise = new RandomNumberGenerator(0);")
                    .AppendLine("  private static RandomNumberGenerator rngObject = new RandomNumberGenerator(0);")
                    .AppendLine()
                    .AppendLine("  private const int INT = 0;")
                    .AppendLine("  private const int FLOAT = 1;");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void InitMapData(Node obj) {
            MapEntry map = (MapEntry)obj;
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine($"  private static int octaves = {printNumber(map.octaves)};")
                   .AppendLine($"  private static float scale = {printNumber(map.scale)};")
                   .AppendLine($"  private static float persistance = {printNumber(map.persistance)};")
                   .AppendLine($"  private static float lacunarity = {printNumber(map.lacunarity)};")
                   .AppendLine()
                   .AppendLine($"  public static float meshHeightMultiplier {{ get; private set; }} = {printNumber(map.mapMultiply)};")
                   .AppendLine($"  public static int mapWidth {{ get; private set; }} = {printNumber(map.width)};")
                   .AppendLine($"  public static int mapHeight {{ get; private set; }} = {printNumber(map.height)};")
                   .AppendLine();
            foreach (ObjBody objBody in map.ObjectBodies) {
                stringBuilder.AppendLine($"  public static {Type.GetType(objBody.type)} {objBody.id}{{ get; private set; }} = {printNumber(objBody.range.lower, objBody.type)};");
            }
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void ObjectIndexConstants(Node obj) {
            ObjEntry objEntry = (ObjEntry) obj;
            StringBuilder stringBuilder = new StringBuilder()
                    .AppendLine($"  private const int {objEntry.id.ToUpper()}INDEX = {printNumber(objectIndex)};");
            objectIndex++;
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void InitObjectBody(Node obj) {
            string name ="";
            if (obj.GetType().Equals(typeof(MapEntry))) {
                MapEntry mapEntry = (MapEntry)obj;
                name = mapEntry.id.ToLower();
            } else if (obj.GetType().Equals(typeof(ObjEntry))) {
                ObjEntry objEntry = (ObjEntry)obj;
                name = objEntry.id.ToLower();
            }
            codeLinesForNoise.Add($"  private static List<VariableData> {name}VariablesData = new List<VariableData>(){{");
        }

        private void CreateObjectBody(Node obj) {
            ObjBody objBody = (ObjBody) obj;
            codeLinesForNoise.Add($"    new VariableData(\"{objBody.id}\", {objBody.type}, {printNumber(objBody.range.lower,objBody.type)}, {printNumber(objBody.range.upper+1,objBody.type)})");
        }

        private void Comma(Node obj) {
            codeLinesForNoise.Add("    ,");
        }

        private void EndObjectBodyOrEntry(Node obj) {
            codeLinesForNoise.Add("  };\n");
        }

        private void InitObjectEntry(Node obj) {
            Program program = (Program) obj;
            codeLinesForNoise.Add("");
            codeLinesForNoise.Add("  private static List<ObjectData> objectDataList = new List<ObjectData>(){");
        }

        private void CreateObjectEntry(Node obj) {
            ObjEntry objEntry = (ObjEntry)obj;
            codeLinesForNoise.Add($"    new ObjectData(\"{objEntry.id}\", {printNumber(objEntry.amount)}, {printNumber(objEntry.radius)}, {printNumber(objEntry.filter.lower)}, {printNumber(objEntry.filter.upper)}, {printNumber(objEntry.rejection)}, {objEntry.id.ToLower()}VariablesData)");
        }


        // Dado un numero lo convierte al string que se debera mostrar en el codigo final 
        private string printNumber(Object number) {
            if (number.GetType().Equals(typeof(float))){
                return number.ToString().Replace(",",".") + "f";
            } else if (number.GetType().Equals(typeof(int))) {
                return number.ToString();
            } else {
                return null;
            }
        }

        // Dado un numero lo convierte al string que se debera mostrar en el codigo final 
        private string printNumber(Object number, Type.Literals type) {
            if (type == Literals.FLOAT){
                return number.ToString().Replace(",",".") + "f";
            } else if (type == Literals.INT) {
                return number.ToString();
            } else {
                return null;
            }
        }

        private void GenerateNoiseMap(Node obj){
            StringBuilder stringBuilder = new StringBuilder()
                    .AppendLine("  /// <summary>")
                    .AppendLine("  /// Returns the generated heightmap for the map at the specified offset.") 
                    .AppendLine("  /// </summary>")
                    .AppendLine("  public static float[,] GenerateNoiseMap(Vector2 offset) {")
                    .AppendLine()
                    .AppendLine("    //Resetting random seed for consistent outputs")
                    .AppendLine("    rngNoise.ResetRng();")
                    .AppendLine()
                    .AppendLine("    float[,] noiseMap = new float[mapWidth,mapHeight];")
                    .AppendLine("    Vector2[] octaveOffsets = new Vector2[octaves];")
                    .AppendLine()
                    .AppendLine("    float maxPossibleHeight = 0;")
                    .AppendLine("    float amplitude = 1;")
                    .AppendLine("    float frequency = 1;")
                    .AppendLine()
                    .AppendLine("    // Calculate each octave offset and the maximum possible height achievable with this map")
                    .AppendLine("    for (int i = 0; i < octaves; i++) {")
                    .AppendLine("      float offsetX = rngNoise.RandomRangeI (-100000, 100000) + offset.x;")
                    .AppendLine("      float offsetY = rngNoise.RandomRangeI (-100000, 100000) + offset.y;")
                    .AppendLine("      octaveOffsets [i] = new Vector2 (offsetX, offsetY);")
                    .AppendLine()
                    .AppendLine("      maxPossibleHeight += amplitude;")
                    .AppendLine("      amplitude *= persistance;")
                    .AppendLine("    }")
                    .AppendLine()  
                    .AppendLine("    float halfWidth = mapWidth / 2f;")
                    .AppendLine("    float halfHeight = mapHeight / 2f;")
                    .AppendLine()
                    .AppendLine("    for (int y = 0; y < mapHeight; y++) {")
                    .AppendLine("      for (int x = 0; x < mapWidth; x++) {")
                    .AppendLine("        amplitude = 1;")
                    .AppendLine("        frequency = 1;")
                    .AppendLine("        float noiseHeight = 0;")
                    .AppendLine()
                    .AppendLine("        for (int i = 0; i < octaves; i++) {")
                    .AppendLine("          float sampleX = (x-halfWidth + octaveOffsets[i].x) / scale * frequency ;")
                    .AppendLine("          float sampleY = (y-halfHeight + octaveOffsets[i].y) / scale * frequency ;")
                    .AppendLine()  
                    .AppendLine("          float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;")
                    .AppendLine("          noiseHeight += perlinValue * amplitude;")
                    .AppendLine("          amplitude *= persistance;")
                    .AppendLine("          frequency *= lacunarity;")
                    .AppendLine("        }")
                    .AppendLine("        float normalizedHeight = (noiseHeight + 1) / (maxPossibleHeight/0.9f);")
                    .AppendLine("        noiseMap [x, y] = Mathf.Clamp(normalizedHeight,0, int.MaxValue);")
                    .AppendLine("      }")
                    .AppendLine("    }")
                    .AppendLine("    return noiseMap;")
                    .AppendLine("  }");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }



        private void GenerateMapValues(Node obj) {
            codeLinesForNoise.Add("  /// <summary>");
            codeLinesForNoise.Add("  /// Generates random values for each map variable.");
            codeLinesForNoise.Add("  /// </summary>");
            codeLinesForNoise.Add("  private static void GenerateMapValues() {");
            MapEntry mapEntry = (MapEntry)obj;
            String typeOfRandomRange = "";
        
            int variableNumber = 0;
            foreach (ObjBody objBody in mapEntry.ObjectBodies) {
                typeOfRandomRange = TypeOfRandomRange(objBody.type);
                codeLinesForNoise.Add($"    {objBody.id} = rngObject.{typeOfRandomRange}(({Type.GetType(objBody.type)}) mapVariablesData[{variableNumber}].begin, ({Type.GetType(objBody.type)})mapVariablesData[{variableNumber}].end);");
                variableNumber++;
            }

            codeLinesForNoise.Add("  }\n");
        }

        private String TypeOfRandomRange(Type.Literals type) {
            if (type == Literals.FLOAT){
                return "RandomRangeF";
            } else if (type == Literals.INT) {
                return "RandomRangeI";
            } else {
                return null;
            }
        }

        private void GenerateObjectsInit(Node obj){
            codeLinesForNoise.Add(
                          "  /// <summary>\n" +
                          "  /// Returns a List of the specified generated objects for the specified heightmap and offset.\n" + 
                          "  /// </summary>\n" +  
                          "  public static MapObject[] GenerateObjects(float[,] heightMap, Vector2 offset, string nameObject) {\n" +
                          "    Vector2[] points;\n" +
                          "    rngObject.CalculateSeed(offset.x, offset.y);\n" +
                          "    switch(nameObject) {");
        }

        private void GenerateObjectsCase(Node obj) {
            ObjEntry objEntry = (ObjEntry)obj;
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine($"     case \"{objEntry.id}\":")
                   .AppendLine($"        points = GeneratePoints({objEntry.id.ToUpper()}INDEX);")
                   .AppendLine($"        List<MapObject> {objEntry.id.ToLower()}List = Generate{objEntry.id}Values(points, {objEntry.id.ToUpper()}INDEX);")
                   .AppendLine($"        {objEntry.id.ToLower()}List = FilterObjects(heightMap, {objEntry.id.ToLower()}List, {objEntry.id.ToUpper()}INDEX);")
                   .Append($"        return {objEntry.id.ToLower()}List.ToArray();");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void GenerateObjectsEnd(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine("      default:")
                   .AppendLine("        return null;")
                   .AppendLine("    }")
                   .AppendLine("  }");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void GenerateValues(Node obj) {
            ObjEntry objEntry = (ObjEntry)obj;
            string name = objEntry.id;
            String line = "";
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine("  /// <summary>")
                   .AppendLine($"  /// Returns a List of generated {name} objects with random values defined by objectDataList.")
                   .AppendLine("  /// </summary>")
                   .AppendLine($"  private static List<MapObject> Generate{name}Values (Vector2[] points, int objectDataIndex) {{")
                   .AppendLine($"    List<{name}Object> new{name} = new List<{name}Object>();");
            foreach (ObjBody objBody in objEntry.objectBodies) {
                stringBuilder
                   .AppendLine($"    {objBody.type.ToString().ToLower()} {objBody.id};");
            }
            stringBuilder
                    .AppendLine("    ObjectData dataFromObject = objectDataList[objectDataIndex];")
                    .AppendLine("    foreach ( Vector2 point in points ){");

            int variableNumber = 0;
            String typeOfRandomRange = "";
            foreach (ObjBody objBody in objEntry.objectBodies) {
                typeOfRandomRange = TypeOfRandomRange(objBody.type);
                stringBuilder
                   .AppendLine($"      {objBody.id} = rngObject.{typeOfRandomRange}(({Type.GetType(objBody.type)})dataFromObject.variables[{variableNumber}].begin, ({Type.GetType(objBody.type)})dataFromObject.variables[{variableNumber}].end);");
                variableNumber++;

            }
            line = line + $"      new{name}.Add(new {name}Object(";
            foreach (ObjBody objBody in objEntry.objectBodies)
            {
                line = line + $"{objBody.id} , ";
            }
            line = line + "point));";
            stringBuilder
                   .AppendLine(line)
                   .AppendLine("    }")
                   .AppendLine($"    return new List<MapObject>(new{name}.ToArray());")
                   .AppendLine("  }");

            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void FilterObjects(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine("  /// <summary>")
                   .AppendLine("  /// Returns the specified List of objects after having been filted by their position in the heightmap defined by objectDataList.")
                   .AppendLine("  /// </summary>")
                   .AppendLine("  private static List<MapObject> FilterObjects(float[,] heightMap, List<MapObject> objects, int objectDataIndex) {")
                   .AppendLine("    int indexX;")
                   .AppendLine("    int indexY;")
                   .AppendLine("    int i = 0;")
                   .AppendLine("    ObjectData dataFromObject = objectDataList[objectDataIndex];")
                   .AppendLine("    while(i < objects.Count) {")
                   .AppendLine("      indexX = (int) Mathf.Floor(objects[i].GetPositionX());")
                   .AppendLine("      indexY = (int) Mathf.Floor(objects[i].GetPositionY());")
                   .AppendLine("      if (!(heightMap[indexX, indexY] >= dataFromObject.minHeight && heightMap[indexX, indexY] <= dataFromObject.maxHeight)){")
                   .AppendLine("        objects.RemoveAt(i);")
                   .AppendLine("      } else {")
                   .AppendLine("        i++;")
                   .AppendLine("      }")
                   .AppendLine("    }")
                   .AppendLine()
                   .AppendLine("    if (dataFromObject.maxAmount >= 0) {")
                   .AppendLine("      i = objects.Count;")
                   .AppendLine("      int index;")
                   .AppendLine("      while (i > dataFromObject.maxAmount) {")
                   .AppendLine("        index = rngObject.RandomRangeI(0, 10000) % objects.Count;")
                   .AppendLine("        objects.RemoveAt(index);")
                   .AppendLine("        i = objects.Count;")
                   .AppendLine("      }")
                   .AppendLine("    }")
                   .AppendLine()
                   .AppendLine("    return objects;")
                   .AppendLine("  }");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void GeneratePoints(Node obj){
            StringBuilder stringBuilder = new StringBuilder()
                   .AppendLine("  /// <summary>")
                   .AppendLine("  /// Returns the coordinates of all the objects to be placed in the map defined by objectDataList.")
                   .AppendLine("  /// </summary>")
                   .AppendLine("  private static Vector2[] GeneratePoints(int objectDataIndex) {")
                   .AppendLine("    int numSamplesBeforeRejection = objectDataList[objectDataIndex].numSamplesBeforeRejection;")
                   .AppendLine("    float radius = objectDataList[objectDataIndex].radius;")
                   .AppendLine()
                   .AppendLine("    float cellSize = radius/Mathf.Sqrt(2);")
                   .AppendLine("    int[,] grid = new int[Mathf.CeilToInt(mapWidth/cellSize), Mathf.CeilToInt(mapHeight/cellSize)];")
                   .AppendLine("    List<Vector2> points = new List<Vector2>();")
                   .AppendLine("    List<Vector2> spawnPoints = new List<Vector2>();")
                   .AppendLine()
                   .AppendLine("    spawnPoints.Add(new Vector2(mapWidth/2, mapHeight/2));")
                   .AppendLine()
                   .AppendLine("    while (spawnPoints.Count > 0) {")
                   .AppendLine("      int spawnIndex = rngObject.RandomRangeI(0,spawnPoints.Count);")
                   .AppendLine("      Vector2 spawnCentre = spawnPoints[spawnIndex];")
                   .AppendLine("      bool candidateAccepted = false;")
                   .AppendLine("      for (int i = 0; i < numSamplesBeforeRejection; i++) {")
                   .AppendLine("        float angle = rngObject.RandomNext() * Mathf.PI * 2;")
                   .AppendLine("        Vector2 dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));")
                   .AppendLine("        Vector2 candidate = spawnCentre + dir * rngObject.RandomRangeF(radius, 2*radius);")
                   .AppendLine("        if (IsValid(candidate, cellSize, radius, points, grid)) {")
                   .AppendLine("          points.Add(candidate);")
                   .AppendLine("          spawnPoints.Add(candidate);")
                   .AppendLine("          grid[(int)(candidate.x/cellSize), (int)(candidate.y/cellSize)] = points.Count;")
                   .AppendLine("          candidateAccepted = true;")
                   .AppendLine("          break;")
                   .AppendLine("        }")
                   .AppendLine("      }")
                   .AppendLine("      if(!candidateAccepted) {")
                   .AppendLine("        spawnPoints.RemoveAt(spawnIndex);")
                   .AppendLine("      }")
                   .AppendLine("    }")
                   .AppendLine()
                   .AppendLine("    return points.ToArray();")
                   .AppendLine("  }")
                   .AppendLine()
                   .AppendLine("  /// <summary>")
                   .AppendLine("  /// Returns a boolean whether the candidate is a valid coordinate for an object to be placed.")
                   .AppendLine("  /// </summary>")
                   .AppendLine("  static bool IsValid(Vector2 candidate, float cellSize, float radius, List<Vector2> points, int[,] grid) {")
                   .AppendLine("    if (candidate.x >=0 && candidate.x < mapWidth && candidate.y >= 0 && candidate.y < mapHeight) {")
                   .AppendLine("      int cellX = (int)(candidate.x/cellSize);")
                   .AppendLine("      int cellY = (int)(candidate.y/cellSize);")
                   .AppendLine("      int searchStartX = Mathf.Max(0,cellX -2);")
                   .AppendLine("      int searchEndX = Mathf.Min(cellX+2,grid.GetLength(0)-1);")
                   .AppendLine("      int searchStartY = Mathf.Max(0,cellY -2);")
                   .AppendLine("      int searchEndY = Mathf.Min(cellY+2,grid.GetLength(1)-1);")
                   .AppendLine()
                   .AppendLine("      for (int x = searchStartX; x <= searchEndX; x++) {")
                   .AppendLine("        for (int y = searchStartY; y <= searchEndY; y++) {")
                   .AppendLine("          int pointIndex = grid[x,y]-1;")
                   .AppendLine("          if (pointIndex != -1) {")
                   .AppendLine("            float sqrDst = (candidate - points[pointIndex]).sqrMagnitude;")
                   .AppendLine("            if (sqrDst < radius*radius) {")
                   .AppendLine("              return false;")
                   .AppendLine("            }")
                   .AppendLine("          }")
                   .AppendLine("        }")
                   .AppendLine("      }")
                   .AppendLine("      return true;")
                   .AppendLine("    }")
                   .AppendLine("    return false;")
                   .AppendLine("  }");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void GenerateSetsGetsAndStructs(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Initializes the relevant map data with the specified seed.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public static void SetMapSeed(int seed){")
                 .AppendLine("    rngNoise.SetSeed(seed);")
                 .AppendLine("    SetRangeSeed(seed);")
                 .AppendLine("    GenerateMapValues();")
                 .AppendLine("    SetRangeSeed(seed);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Reinitializes the object related RNG with the specified seed.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public static void SetRangeSeed(int seed){")
                 .AppendLine("    rngObject.SetSeed(seed);")
                 .AppendLine("  }")
                 .AppendLine("}")
                 .AppendLine()
                 .AppendLine("public struct ObjectData {")
                 .AppendLine("  public readonly string name;")
                 .AppendLine("  public readonly int maxAmount;")
                 .AppendLine("  public readonly float radius;")
                 .AppendLine("  public readonly float minHeight;")
                 .AppendLine("  public readonly float maxHeight;")
                 .AppendLine("  public readonly int numSamplesBeforeRejection;")
                 .AppendLine("  public List<VariableData> variables;")
                 .AppendLine()
                 .AppendLine("  public ObjectData (string name, int maxAmount, float radius, float minHeight, float maxHeight,")
                 .AppendLine("                     int numSamplesBeforeRejection, List<VariableData> variables) {")
                 .AppendLine("    this.name = name;")
                 .AppendLine("    this.maxAmount = maxAmount;")
                 .AppendLine("    this.radius = radius;")
                 .AppendLine("    this.minHeight = minHeight;")
                 .AppendLine("    this.maxHeight = maxHeight;")
                 .AppendLine("    this.variables = variables;")
                 .AppendLine("    this.numSamplesBeforeRejection = numSamplesBeforeRejection;")
                 .AppendLine("  }")
                 .AppendLine("}")
                 .AppendLine()
                 .AppendLine("public struct VariableData {")
                 .AppendLine("  public readonly string name;")
                 .AppendLine("  public readonly int type;")
                 .AppendLine("  public readonly float begin;")
                 .AppendLine("  public readonly float end;")
                 .AppendLine()
                 .AppendLine("  public VariableData (string name, int type, float begin, float end) {")
                 .AppendLine("    this.name = name;")
                 .AppendLine("    this.type = type;")
                 .AppendLine("    this.begin = begin;")
                 .AppendLine("    this.end = end;")
                 .AppendLine("  }")
                 .AppendLine("}");
            codeLinesForNoise.Add(stringBuilder.ToString());
        }

        private void CreateClassMapObject(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                 .AppendLine("using UnityEngine;")
                 .AppendLine()
                 .AppendLine("/// <summary>")
				 .AppendLine("/// This class stores data relating to any map object generated by the Noise class.")
				 .AppendLine("/// </summary>")
                 .AppendLine("public class MapObject {")
                 .AppendLine("  protected Vector2 position;")
                 .AppendLine()
                 .AppendLine("  public MapObject (Vector2 position){")
                 .AppendLine("    this.position = position;")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  public float GetPositionX(){")
                 .AppendLine("    return position.x;")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  public float GetPositionY(){")
                 .AppendLine("    return position.y;")
                 .AppendLine("  }")
                 .AppendLine("}");
            codeLinesForMapObject.Add(stringBuilder.ToString());
        }

        private void CreateClassObjects(Node obj) {
            ObjEntry objEntry = (ObjEntry)obj;
            StringBuilder stringBuilder = new StringBuilder()
            	 .AppendLine("/// <summary>")
				 .AppendLine("/// Class that inherents MapObject and contains the relative information of any forest object. TODO::forest cambia segun el objeto")
				 .AppendLine("/// </summary>")
                 .AppendLine($"public class {objEntry.id}Object : MapObject {{");
 
            foreach (ObjBody objBody in objEntry.objectBodies) {
                stringBuilder.AppendLine($"  public {objBody.type.ToString().ToLower()} {objBody.id};");
            }
            String line = $"\n  public {objEntry.id}Object (";
           
            foreach (ObjBody objBody in objEntry.objectBodies) {
                line = line + $"{objBody.type.ToString().ToLower()} {objBody.id} , ";
            }
            line = line + "Vector2 position) : base(position){";
            stringBuilder.AppendLine(line);
            foreach (ObjBody objBody in objEntry.objectBodies) {
                stringBuilder.AppendLine($"    this.{objBody.id} = {objBody.id};");                         
            }
            stringBuilder
                .AppendLine("  }")
                .AppendLine("}");
            codeLinesForMapObject.Add(stringBuilder.ToString());
        }

        private void CreateRandomNumberGenerator(Node obj) {
            StringBuilder stringBuilder = new StringBuilder()
                 .AppendLine("using System;")
                 .AppendLine("using System.Collections;")
                 .AppendLine("using System.Collections.Generic;")
                 .AppendLine()
            	 .AppendLine("public class RandomNumberGenerator {")
                 .AppendLine()
            	 .AppendLine("  private int seed;")
                 .AppendLine("  private System.Random rngRange;")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Class Constructor")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public RandomNumberGenerator(int seed) {")
                 .AppendLine("    SetSeed(seed);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Returns a random integer between and including the specified values.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public int RandomRangeI(int minimum, int maximum) {")
                 .AppendLine("    return rngRange.Next(minimum, maximum);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Returns a random float between and including the specified values.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public float RandomRangeF(float minimum, float maximum) {")
                 .AppendLine("    return (float) rngRange.NextDouble() * (maximum - minimum) + minimum;")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Returns a random integer.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public int RandomNext(){")
                 .AppendLine("    return rngRange.Next();")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Resets the generator with the localy stored seed.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public void ResetRng(){")
                 .AppendLine("    rngRange = new System.Random(this.seed);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Resets the generator with the specified seed.")
                 .AppendLine("  /// </summary>")
                 .AppendLine("  public void UseSeed(int seed){")
                 .AppendLine("    rngRange = new System.Random(seed);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Resets the generator with the specified seed and sorts it localy.")
                 .AppendLine("  /// <summary>")
                 .AppendLine("  public void SetSeed(int seed){")
                 .AppendLine("    this.seed = seed;")
                 .AppendLine("    rngRange = new System.Random(seed);")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Returns the seed stored localy.")
                 .AppendLine("  /// <summary>")
                 .AppendLine("  public int GetSeed(){")
                 .AppendLine("    return seed;")
                 .AppendLine("  }")
                 .AppendLine()
                 .AppendLine("  /// <summary>")
                 .AppendLine("  /// Resets the generator with the seed corresponding to the specified coordinates.")
                 .AppendLine("  /// Each coordiante has a unique seed.")
                 .AppendLine("  /// <summary>")
                 .AppendLine("  public void CalculateSeed(float offX, float offY) {")
                 .AppendLine()
                 .AppendLine("    int x = (int) offX;")
                 .AppendLine("    int y = (int) offY;")
                 .AppendLine()
                 .AppendLine("    // Calculate how far away we are from the center of the grid")
                 .AppendLine("    int level = Math.Max(Math.Abs(x), Math.Abs(y));")
                 .AppendLine("    if (level == 0) {")
                 .AppendLine("      UseSeed(this.seed);")
                 .AppendLine("    }")
                 .AppendLine("    //Highest value possible on the current level")
                 .AppendLine("    int maxLevelValue = (int) Math.Sqrt((level*2)+1)-1;")
                 .AppendLine("    if (x == level && y != -level) {")
                 .AppendLine("      UseSeed(this.seed + maxLevelValue - level*6 - level - y);")
                 .AppendLine("    }")
                 .AppendLine("    if (x == -level && y != level) {")
                 .AppendLine("      UseSeed(this.seed + maxLevelValue - level*2 - level + y);")
                 .AppendLine("    }")
                 .AppendLine("    if (y == level && x != level) {")
                 .AppendLine("      UseSeed(this.seed + maxLevelValue - level*4 - level + x);")
                 .AppendLine("    }")
                 .AppendLine("    // if (y == -level && x != -level) // Last Case Scenario")
                 .AppendLine("    UseSeed(this.seed + maxLevelValue - level - x);")
                 .AppendLine("  }")
                 .AppendLine("}");

            codeLinesForRandomNumberGenerator.Add(stringBuilder.ToString());
        }

    }
}    