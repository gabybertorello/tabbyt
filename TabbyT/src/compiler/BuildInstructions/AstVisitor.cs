﻿using TabbyT.src.compiler;
internal abstract class AstVisitor<T>{

  public abstract T Visit(TabbyT.src.compiler.Program p);
  public abstract T Visit(MapEntry m);
  public abstract T Visit(ObjEntry oe, string typeOfEntry);
  public abstract T Visit(ObjBody ob);
  public abstract T Visit(Range r);

  public T Visit(Node node){
    return Visit((dynamic)node);
  }
}

