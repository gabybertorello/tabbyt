﻿using System.Collections.Generic;

namespace TabbyT.src.compiler {

  internal abstract class Node {
    public string name;
  }

  internal class Program : Node {
    public List<ObjEntry> ObjectEntries { get; }
    public MapEntry mapEntry { get; }

    public Program (List<ObjEntry> ObjectEntry, MapEntry mapEntry) {
      this.name = "Program";
      this.ObjectEntries = ObjectEntry;
      this.mapEntry = mapEntry;
    }
  }

  internal class ObjEntry : Node {
    public string id { get; }
    public int amount { get; }
    public List<ObjBody> objectBodies { get; }
    public float radius { get; }
    public Range filter { get; }
    public int rejection { get; set; }
    public int objectNumber { get; set; }


    public ObjEntry (string id, int amount, List<ObjBody> objectBody, 
                  float radius, Range filter, int rejection) {
      this.name = "ObjEntry";
      this.id = id;
      this.amount = amount;
      this.objectBodies = objectBody;
      this.rejection = rejection;
      this.radius = radius;
      this.filter = filter;

    }
  }

  internal class MapEntry : Node {
    public string id { get; }
    public List<ObjBody> ObjectBodies { get; }
    public int height { get; }
    public int width { get; }
    public float mapMultiply { get; }
    public float scale { get; }
    public int octaves { get; }
    public float persistance { get; }
    public float lacunarity { get; }

    public MapEntry( string id,List<ObjBody> objectBody, int height, int width, float mapMultiply,
                  float scale, int octaves, float persistance, float lacunarity) {
      this.name = "MapEntry";
      this.id = id;
      this.ObjectBodies = objectBody;
      this.height = height;
      this.width = width;
      this.mapMultiply = mapMultiply;
      this.scale = scale;
      this.octaves = octaves;
      this.persistance = persistance;
      this.lacunarity = lacunarity;
    }
  }

  internal class ObjBody : Node {
    public string id { get; }
    public Type.Literals type;
    public Range range;
    public int objectNumber { get; set; }

    public ObjBody( string id, Type.Literals type, Range range ) {
      this.name = "ObjBody";
      this.id = id;
      this.type = type;
      this.range = range;

    }

    public ObjBody( string id, Type.Literals type, float num ) {
      this.id = id;
      this.type = type;
      if (num >= 0) {
        this.range = new Range(0, num);
      } else {
        this.range = new Range(num, 0);
      }
    }
  }
  
  internal class Range : Node {
    public float lower { get; }
    public float upper { get; }

    public Range (float lower, float upper) {
      this.name = "Range";
      this.lower = lower;
      this.upper = upper;
    }
  }

  public class Type {
    public enum Literals { INT,FLOAT, NULL }
    public static Literals GetLiteral(string word) {
      switch (word) {
        case "int":
          return Literals.INT;
        case "float":
          return Literals.FLOAT;
        default:
          return Literals.NULL;
      }
    }

    public static string GetType(Literals lit) {
      switch (lit) {
        case Literals.INT:
          return "int";
        case Literals.FLOAT:
          return "float";
        default:
          return "void";
      }
    }
  }
}