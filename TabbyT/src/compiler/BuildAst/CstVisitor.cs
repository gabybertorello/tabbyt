﻿using TabbyT.src.compiler;
using static TabbyParser;

internal abstract class CstVisitor<T>
{
  public abstract T Visit(ProgramContext program);
  public abstract T Visit(MapentryContext mapEntry);
  public abstract T Visit(ObjentryContext objEntry);
  public abstract T Visit(ObjbodyContext objBody);
  public abstract T Visit(RangeContext range);

  public T Visit (Node node)
  {
    return Visit((dynamic)node);
  }
}

