﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Antlr4.Runtime.Tree;
using TabbyT.src.compiler.Error;
using static TabbyParser;

/// <summary>
/// Given a CST, an AST is created that allows for a 
/// simplified representation of the source code.
/// </summary>
namespace TabbyT.src.compiler
{
  internal class BuildAstVisitor : CstVisitor<Node> {
    private const int DEFAULT_AMOUNT_OBJECTS = 50;
    private const int DEFAULT_MAP_SIZE = 241;
    private const float DEFAULT_MAP_MULTIPLY = 34f;
    private const float DEFAULT_SCALE = 45f;
    private const int DEFAULT_OCTAVES = 8;
    private const float DEFAULT_PERSISTENCE = 0.5f;
    private const float DEFAULT_LACUNARITY = 2f;
    private const float DEFAULT_RADIUS = 6f;
    private const int DEFAULT_REJECTION = 8;
    private const float DEFAULT_RANGE_LOWER = 0f;
    private const float DEFAULT_RANGE_UPPER = 10f;

    ErrorList errorList;

    public BuildAstVisitor() {
      errorList = ErrorList.getErrorList();
    }

    /// <summary>
    /// Given a ProgramContext node from the CST, return 
    /// a Program node from the AST based off it.
    /// </summary>
    public override Node Visit(ProgramContext programContext) {
      //Node Data
      List<ObjEntry> objectEntries = new List<ObjEntry>();
      MapEntry map;
      //Node Data

      MapentryContext mapEntry = programContext.mapentry();
      ObjentryContext[] objectEntriesContext = programContext.objentry();
      List<string> names = new List<string>();

      int i = 0;
      foreach (var objectEntryContext in objectEntriesContext) {
        ObjEntry objEntry = (ObjEntry)Visit(objectEntryContext);
        objEntry.objectNumber = i;
        i++;
        objectEntries.Add(objEntry);
      }
      map = ((MapEntry)Visit(mapEntry));

      AssertWithoutDuplicates(objectEntries, "Error: Duplicate objEntry names.");

      Program program = new Program(objectEntries, map);

      return program;
    }

    /// <summary>
    /// Given an ObjentryContext node from the CST, return 
    /// an ObjEntry node from the AST based off it.
    /// </summary>  
    public override Node Visit(ObjentryContext objEntryContext) {
      //Node Data
      String id = objEntryContext.ID().GetText();
      int amount;
      int rejection;
      List<ObjBody> objectBodies = new List<ObjBody>();
      float radius;
      Range filter;
      //End Node Data

      ObjbodyContext[] objectBodiesContext = objEntryContext.objbody();
      RadiusContext[] radiusContext = objEntryContext.radius();
      FilterContext[] filterContext = objEntryContext.filter();
      RejectionContext[] rejectionContext = objEntryContext.rejection();

      AssertAmountOfReservedVariables(radiusContext.ToList().Count(), "Radius");
      AssertAmountOfReservedVariables(filterContext.ToList().Count(), "Filter");
      AssertAmountOfReservedVariables(rejectionContext.ToList().Count(), "Rejection");

      objectBodies = GetObjectBodyList(objectBodiesContext);
      AssertWithoutDuplicates(objectBodies, "Error: Duplicate objBody names.");

      amount = objEntryContext.integer() == null ? DEFAULT_AMOUNT_OBJECTS : IntegerCheck(objEntryContext.integer());

      rejection = rejectionContext.ToList().Count() == 0 ? DEFAULT_REJECTION : IntegerCheck(rejectionContext.First().integer());

      radius = radiusContext.ToList().Count() == 0 ? DEFAULT_RADIUS : NumberCheck( radiusContext.First().number());

      filter = filterContext.ToList().Count() == 0 ? new Range(DEFAULT_RANGE_LOWER,DEFAULT_RANGE_UPPER) : ((Range)Visit(filterContext.First().range()));

      AssertGreaterThanMinusOne(amount, "Amount", id);
      AssertGreaterThanZero(radius, "Radius", id);
      AssertNumberInRangeValue(filter.lower, "Filter lower", id);
      AssertNumberInRangeValue(filter.upper, "Filter upper", id);
      AssertGreaterThanOne(rejection, "Rejection", id);

      AssertLesserThan(filter.lower, filter.upper, id);
      AssertDifferentValues(filter.lower, filter.upper, id);
      ObjEntry objEntry = new ObjEntry(id, amount , objectBodies, radius, filter, rejection);
      return objEntry;

    }


    /// <summary>
    /// Given a MapentryContext node from the CST, return 
    /// a MapEntry node from the AST based off it.
    /// </summary>  
    public override Node Visit(MapentryContext mapEntryContext) {
      //Node Data
      string id = "Map";
      List<ObjBody> objectBodies = new List<ObjBody>();
      int height;
      int width;
      float mapMultiply;
      float scale;
      int octaves;
      float persistance;
      float lacunarity;
      //End Node Data

      // Obtain parser data
      ObjbodyContext[] objectBodiesContext = mapEntryContext.objbody();
      MapmultiplyContext[] mapMultipyContext = mapEntryContext.mapmultiply();
      ScaleContext[] scaleContext = mapEntryContext.scale();
      OctavesContext[] octavesContext = mapEntryContext.octaves();
      PersistanceContext[] persistanceContext = mapEntryContext.persistance();
      LacunarityContext[] lacunarityContext = mapEntryContext.lacunarity();

      AssertAmountOfReservedVariables(mapMultipyContext.ToList().Count(), "MapMultiply");
      AssertAmountOfReservedVariables(scaleContext.ToList().Count(), "Scale");
      AssertAmountOfReservedVariables(octavesContext.ToList().Count(), "Octaves");
      AssertAmountOfReservedVariables(persistanceContext.ToList().Count(), "Persistance");
      AssertAmountOfReservedVariables(lacunarityContext.ToList().Count(), "Lacunarity");

      objectBodies = GetObjectBodyList(objectBodiesContext);
      AssertWithoutDuplicates(objectBodies, "Error: Duplicate objBody names.");
      height = mapEntryContext.tuple() == null ? DEFAULT_MAP_SIZE : Int32.Parse(mapEntryContext.tuple().INTEGER(0).GetText());
      width = mapEntryContext.tuple() == null ? DEFAULT_MAP_SIZE : Int32.Parse(mapEntryContext.tuple().INTEGER(1).GetText());
   
      mapMultiply = mapMultipyContext.ToList().Count() == 0 ? DEFAULT_MAP_MULTIPLY : NumberCheck(mapMultipyContext.First().number());

      scale = scaleContext.ToList().Count() == 0 ? DEFAULT_SCALE : NumberCheck(scaleContext.First().number());

      octaves = octavesContext.ToList().Count() == 0 ? DEFAULT_OCTAVES : IntegerCheck(octavesContext.First().integer());

      persistance = persistanceContext.ToList().Count() == 0 ? DEFAULT_PERSISTENCE : NumberCheck(persistanceContext.First().number());

      lacunarity = lacunarityContext.ToList().Count() == 0 ? DEFAULT_LACUNARITY : NumberCheck(lacunarityContext.First().number());

      AssertValueBetweenOneAndTwoHundredFiftyFive(height, "Height", id);
      AssertValueBetweenOneAndTwoHundredFiftyFive(width, "Width", id);
      AssertGreaterThanZero(scale, "Scale", id);
      AssertGreaterThanZero(octaves, "Octaves", id);
      AssertValueBetweenZeroAndOne(persistance, "Persistance", id);
      AssertGreaterOrEqualThanOne(lacunarity, "Lacunarity", id);

      MapEntry mapEntry = new MapEntry(id,objectBodies, height, width, mapMultiply, scale, octaves, persistance, lacunarity);

      return mapEntry;
    }

    /// <summary>
    /// Given an ObjbodyContext node from the CST, return 
    /// an ObjBody node from the AST based off it.
    /// </summary>  
    public override Node Visit(ObjbodyContext objBodyContext) {
      //Node Data
      Type.Literals type = Type.GetLiteral(objBodyContext.type().GetText());
      String id = objBodyContext.ID().GetText();
     //End Node Data

      NumberContext num = objBodyContext.number();
      RangeContext rangeContext = objBodyContext.range();
      ObjBody objBody = null;

      if (rangeContext != null & num == null) {
        objBody = new ObjBody(id, type, (Range)Visit(rangeContext));
        AssertLesserThan(objBody.range.lower, objBody.range.upper, id);

      } else if (rangeContext == null & num != null) {
        objBody = new ObjBody(id, type, NumberCheck(num));

      } else {
        objBody = new ObjBody(id, type, DEFAULT_RANGE_UPPER);
      }
      AssertDifferentValues(objBody.range.lower,objBody.range.upper, id);

      return objBody;
    }

    /// <summary>
    /// Given a RangeContext node from the CST, return 
    /// a Range node from the AST based off it.
    /// </summary> 
    public override Node Visit(RangeContext rangeContext) {
      //Node Data
      float lower = NumberCheck(rangeContext.number(0));
      float upper = NumberCheck(rangeContext.number(1));
      //End Node Data

      Range rangeObj = new Range(lower, upper);

      return rangeObj;
    }

    /// <summary>
    /// Given a NumberContext, verify if the value is an integer 
    /// or a real number and if it is positive or negative. 
    /// Then return said value as a float.
    /// </summary> 
    private float NumberCheck(NumberContext numberContext) {
      float number = 0;
      if (numberContext.INTEGER() == null) {
        number = float.Parse(numberContext.REAL().GetText(), CultureInfo.InvariantCulture.NumberFormat);
      }

      if (numberContext.REAL() == null) {
        number = float.Parse(numberContext.INTEGER().GetText(), CultureInfo.InvariantCulture.NumberFormat);
      }

      if (numberContext.SYMBOL() != null) {
        number = number * -1;
      }
      return number;
    }

    /// <summary>
    /// Given a IntegerContext, verify if it is positive or negative. 
    /// Then return said value as a integer.
    /// </summary> 
    private int IntegerCheck(IntegerContext integerContext) {
      int number = (int)float.Parse(integerContext.INTEGER().GetText(), CultureInfo.InvariantCulture.NumberFormat);

      if (integerContext.SYMBOL() != null) {
        number = number * -1;
      }
      return number;
    }

    /// <summary>
    /// Given a list of ObjbodyContext (CST nodes)
    /// return a list of objBody (AST nodes) based off them.
    /// </summary> 
    private List<ObjBody> GetObjectBodyList(ObjbodyContext[] objectBodiesContext) {
      List<ObjBody> objectBodies = new List<ObjBody>();
      int i = 0;
      foreach (var objectBodyContext in objectBodiesContext) {
        ObjBody objBody = (ObjBody)Visit(objectBodyContext);
        objBody.objectNumber = i;
        i++;
        objectBodies.Add(objBody);
      }
      return objectBodies;
    }



    private void AssertAmountOfReservedVariables(int number, String name) {
      if (number != 1 && number != 0){
        errorList.Add($"Error: There are {number} {name} declared. There should be no more than one.");
      }
    }

 
    private void AssertWithoutDuplicates(List<ObjEntry> objEntries, string msj) {
      List<string> names = new List<string>();
      foreach (ObjEntry objEntry in objEntries) {
        names.Add(objEntry.id);
      }
      HashSet<string> namesWithoutDuplicates = new HashSet<String>(names);
      if (names.Count != namesWithoutDuplicates.Count) {
        errorList.Add(msj);
      }
    }

    private void AssertWithoutDuplicates(List<ObjBody> objBodies, string msj) {
      List<string> names = new List<string>();
      foreach (ObjBody objBody in objBodies) {
        names.Add(objBody.id);
      }
      HashSet<string> namesWithoutDuplicates = new HashSet<String>(names);
      if (names.Count != namesWithoutDuplicates.Count){
        errorList.Add(msj);
      }
    }

    private void AssertGreaterThanZero(float value, string msj, string nameObj ) {
      if (value <= 0) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which is equal or lesser than zero.");
      }
    }  

    private void AssertGreaterThanMinusOne(float value, string msj, string nameObj) {
      if (value < -1) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which is lesser than minus one.");
      }
    }
    private void AssertNumberInRangeValue(float value, string msj, string nameObj) {
      if (!(0 <= value & value <= float.MaxValue)) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which should be a value between [0:{float.MaxValue}].");
      }
    }

    private void AssertLesserThan(float lower, float upper, string nameObj) {
      if (lower > upper) {
        errorList.Add($"Error: In the object {nameObj}, the value {lower} is greater than the value {upper}");
      }
    }

    private void AssertValueBetweenZeroAndOne(float value, string msj, string nameObj ){
      if (!(0 < value & value < 1)) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which should be a value between (0,1).");
      }
    }

    private void AssertValueBetweenOneAndTwoHundredFiftyFive(float value, string msj, string nameObj){
       if (!(1 <= value & value <= 255)) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which should be a value between [1,255].");
      }
    }

    private void AssertGreaterOrEqualThanOne(float value, string msj, string nameObj) {
      if (value <= 1) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which is equal or lesser than one.");
      }
    }

     private void AssertGreaterThanOne(int value, string msj, string nameObj) {
      if (value < 1) {
        errorList.Add($"Error: The variable {msj} of the object {nameObj} has a value of {value} which is lesser than one.");
      }
    }

    private void AssertDifferentValues(float lower, float upper, string nameObj){
      if (lower == upper) {
        errorList.Add($"Error: In the object {nameObj}, the value {lower} must be different from the value {upper}");
      }
    }


  }
}
