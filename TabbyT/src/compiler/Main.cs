﻿/*
Autores:
    - Cejuela, Max
    - Bertorello, Gabriela
*/

using System.IO;
using System.Text;
using TabbyT.src.compiler;
using Antlr4.Runtime;
using TabbyT.src.compiler.Error;
using System;

public class Program {

  public static void Main(string[] args) {

    // Read the input file
    ErrorList errorList = ErrorList.getErrorList();
    TabbyParser.ProgramContext cst = null;
    string[] file = {""};
    string output =args[1];
    try {
      file = System.IO.File.ReadAllLines(args[0]);
    } catch (IOException){
      errorList.Add("Incorrect file location: " + args[0]);
    }
    string stringFile = string.Concat(file);
    //End Read the input file

    // Creation of the CST
    ICharStream inputStream = new AntlrInputStream(new MemoryStream(Encoding.ASCII.GetBytes(stringFile)));
    TabbyLexer lexer = new TabbyLexer((ICharStream)inputStream);
    CommonTokenStream tokens = new CommonTokenStream((ITokenSource)lexer);
    TabbyParser parser = new TabbyParser((ITokenStream)tokens);
    parser.RemoveErrorListeners();
    try {
      parser.AddErrorListener(ThrowingErrorListener.INSTANCE);
      cst = parser.program();
    } catch (ParsingException ex) {
      errorList.Add($"file {args[0]} error in {ex.msg}");
    }
    //End Creation of the CST

    // Creation of the AST
    Node ast = new BuildAstVisitor().Visit(cst);

    // Creation of the Intermediate Instructions
    var instructions = new BuildInstructions().Visit(ast);

    // Creation of the source code
    new ClassGenerator(instructions, output);

    Console.WriteLine($"Compilation has been successful. Files are located in the folder: {output}");
  }
    
}