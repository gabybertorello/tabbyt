 grammar Tabby;

/*-----------------------------------------------------------
 *    REGLAS SINTACTICAS
 *----------------------------------------------------------- */

 program     : ( BOXOPEN objentry BOXCLOSE )* ( BOXOPEN mapentry BOXCLOSE ) ( BOXOPEN objentry BOXCLOSE )*;
 mapentry    : MAP COLON tuple? ( objbody | mapmultiply | scale | octaves | persistance | lacunarity ) ( COMMA ( objbody | mapmultiply | scale | octaves | persistance | lacunarity ) )* ;
 objentry    : OBJECT ID COLON integer? ( objbody | radius | filter | rejection ) ( COMMA ( objbody | radius | filter | rejection) )* ;
 objbody     : type ID ( COLON ( number | range ) )?;
 type        : INT | FLOAT;
 range       : PARENTHOPEN number COLON number PARENTHCLOSE;
 number      : SYMBOL? ( INTEGER | REAL );
 integer     : SYMBOL? INTEGER;
 tuple       : PARENTHOPEN INTEGER COMMA INTEGER PARENTHCLOSE;
 
 // Keywords
 radius      : RADIUS COLON number;
 filter      : FILTER COLON range;
 rejection   : REJECTION COLON integer;
 
 mapmultiply : MAPMULTIPLIER COLON number;
 scale       : SCALE COLON number;
 octaves     : OCTAVES COLON integer;
 persistance : PERSISTANCE COLON number;
 lacunarity  : LACUNARITY COLON number;
 
 /*-----------------------------------------------------------
 *    REGLAS LEXICAS
 *----------------------------------------------------------- */
 
 DEF           : 'def';
 OBJECT        : 'Object';
 MAP           : 'Map';
 MAPMULTIPLIER : 'mapMultiplier';
 RADIUS        : 'radius';
 FILTER        : 'filter';
 REJECTION     : 'rejection';
 SCALE         : 'scale';
 OCTAVES       : 'octaves';
 PERSISTANCE   : 'persistance';
 LACUNARITY    : 'lacunarity';
 COLON         : ':';
 BOXOPEN       : '[';
 BOXCLOSE      : ']';
 PARENTHOPEN   : '(';
 PARENTHCLOSE  : ')';
 COMMA         : ',';
 INT           : 'int';
 FLOAT         : 'float';
 BOOL          : 'bool';
 ENUM          : 'enum';
 LIST          : 'list';
 SYMBOL        : '-';
 INTEGER       : '0' | ('1'..'9') ('0'..'9')*;
 REAL          : ('0'..'9')* '.' ('0'..'9')+; //( NUM ( DOT NUM? )? | DOT NUM );
 ID            : (('a'..'z') | ('A'..'Z') | ('0'..'9') | '_')+;
 WS            : ( ' ' | '\t' | '\r'| '\n' ) 
 -> skip
 //{$channel=HIDDEN;}
 ;
 