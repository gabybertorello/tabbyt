TabbyT es una herramienta para facilitar la generación procedural de terrenos en tres dimensiones y la disposición de elementos sobre los mismos.  
La misma dada, una especificación, brinda una librería que contiene 3 archivos:
	- Noise.cs: Módulo que se encarga de generar los datos del terreno y la información de los objetos sobre ella.
		* Brinda un algoritmo de ruido para facilitar la creación de terrenos aleatorios.
		* Ofrece un sistema de generación de objetos aleatorios con restricciones definidas por el usuario.
	- MapObject.cs: Instancias de información de los objetos generados por la herramienta.
	- RandomNumberGenerator.cs: Módulo que se encarga de manejar la generación de números aleatorios.

----------------Windows----------------
Windows:
1- Instalar Mono 64-bit desde la pagina: 
https://www.mono-project.com/download/stable/#download-win

2- Instalar Visual Studio Build Tools (Permite crear proyectos desde linea de comando sin tener que instalar el entorno de visual studio)
link de descarga: https://visualstudio.microsoft.com/downloads/?q=build+tools
NOTA:: Asegurar que el path para el commando CSC este actualizado al de build tools y no al CSC obsoleto que se encuentra en WINDOWS\Microsoft.NET\Framework\v4.0.30319.

3- Instalar el Java JDK más reciente

4- Clonar repositorio desde la pagina:
git clone https://gabybertorello@bitbucket.org/gabybertorello/tabbyt.git

5- Ingresar a .\TabbyT

6- Correr el script compile.bat
Primero se crean los archivos autogenerados de parser y lexer desde la gramatica y se compila TabbyT.

7- Correr el script test.bat
Se corre utilizando como archivo de entrada .\test\test.tb y como carpeta de salida .\result
Si se desea cambiar el archivo de entrada o la carpeta de salida solo se tiene que modificar el script

----------------macOS----------------
macOS:
1- Instalar Mono (Stable channel) desde la pagina: 
https://www.mono-project.com/download/stable/#download-mac
NOTA:: Asegurar de agregar al path la direccion "/Library/Frameworks/Mono.framework/Versions/Current/bin".

2- Instalar el Java JDK más reciente

3- Clonar repositorio desde la pagina:
git clone https://gabybertorello@bitbucket.org/gabybertorello/tabbyt.git

4- Ingresar a ./TabbyT

5- Correr el script compile.sh
Primero se crean los archivos autogenerados de parser y lexer desde la gramatica y se compila TabbyT.

6- Correr el script test.sh
Se corre utilizando como archivo de entrada ./test/test.tb y como carpeta de salida ./result
Si se desea cambiar el archivo de entrada o la carpeta de salida solo se tiene que modificar el script

----------------Linux----------------
Linux:
1- Instalar Mono (Stable channel) desde la pagina: 
https://www.mono-project.com/download/stable/#download-lin

2- Instalar el Java JDK más reciente

3- Clonar repositorio desde la pagina:
git clone https://gabybertorello@bitbucket.org/gabybertorello/tabbyt.git

4- Ingresar a ./TabbyT

5- Correr el script compile.sh
Primero se crean los archivos autogenerados de parser y lexer desde la gramatica y se compila TabbyT.

6- Correr el script test.sh
Se corre utilizando como archivo de entrada ./test/test.tb y como carpeta de salida ./result
Si se desea cambiar el archivo de entrada o la carpeta de salida solo se tiene que modificar el script
