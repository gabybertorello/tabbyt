# Create the parser and lexer using the Tabby.g4 grammer.
java -jar src/grammar/lib/antlr-4.7.1-complete.jar -Dlanguage=CSharp src/grammar/Tabby.g4

# Create the folder src/compiler/ParserLexerGeneration
mkdir -p src/compiler/ParserLexerGeneration

# Move the generated parser and lexer to the folder src/compiler/ParserLexerGeneration.
cd src/grammar
mv Tabby.interp Tabby.tokens TabbyBaseListener.cs TabbyLexer.cs TabbyLexer.interp TabbyLexer.tokens TabbyListener.cs TabbyParser.cs ../compiler/ParserLexerGeneration
cd ../..

# Compile the TabbyT program with the Antlr4 library.
csc src/compiler/Main.cs src/compiler/BuildAst/*.cs src/compiler/BuildInstructions/*.cs src/compiler/Error/*.cs src/compiler/NodeAst/*.cs src/compiler/Error/*.cs src/compiler/ParserLexerGeneration/*.cs -r:Antlr4.Runtime.Standard.dll -out:TabbyT.exe

read -n 1 -r -s -p $'Press any key to continue...\n'